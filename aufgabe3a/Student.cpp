#include <iostream>
#include <string.h>

using namespace std;

class Student {


public:
    Student(char* name, int matrikelNr, int studienbeginn)
    {
        strcpy(nameStudent, name);
        matrikelNummer = matrikelNr;
        jahrStudeinBeginn = studienbeginn;
    }

    void AnzeigeAllerDaten(void) {

        cout << "### ### Output Student ### ###" << endl
                << "Name: " << nameStudent << endl
                << "Matrikelnummer: " << matrikelNummer << endl
                << "Studienbeginn: " << jahrStudeinBeginn << endl;
        cout << endl;
    }

    void EingabeAllerDaten(void) {

        string input = "";
        cout << "### ### Input new student ### ###" << endl;
        cout << "Name: ";
        cin >> nameStudent;

        cout << "Matrikelnummer:";
        cin >> matrikelNummer;

        cout << "Studienbegin Jahr:";
        cin >> jahrStudeinBeginn;
        cout << endl;
    }

protected:
    char nameStudent[40];
    unsigned long int matrikelNummer = 0L;
    unsigned int jahrStudeinBeginn = 0;
};