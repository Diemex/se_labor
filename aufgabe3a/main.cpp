#include <iostream>
#include "Student.cpp"

using namespace std;


int main() {
    Student myStudent1 ("Max", 123UL, 456);
    Student myStudent2 ("Musterman", 333UL, 999);
//    myStudent1->EingabeAllerDaten();
//    myStudent2->EingabeAllerDaten();
    myStudent1.AnzeigeAllerDaten();
    myStudent2.AnzeigeAllerDaten();
}