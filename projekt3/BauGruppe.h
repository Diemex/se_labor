#pragma once

#include "stdafx.h"

class CBauGruppe : public CKomponente{
public:
    CBauGruppe(void){
    }

    double getPreis();

    CBauteil *get(int pos);

    void ausgabeBauteile();

    void addiereBauteil(CBauteil *bauteil, int anzahl);

    void loescheBauteil(CBauteil *bauteil);

protected:
    array<CKomponenteAnzahl<CBauteil>, 10> mKomponenten;
};