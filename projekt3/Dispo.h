#pragma once

class CDispo {
public:
    CDispo(void);

    void startProjekt();

    void bauteileAusgeben();

    CBauteil *waehleBauteil();

    CBauGruppe *waehleBaugruppe();

    CGeraet *waehleGeraet();

    void infoBauteil();

    void infoBaugruppe();

    void infoGeraet();

    void bearbeiteBaugruppe();

    void bearbeiteGeraet();

protected:
    CGeraet mGeraete[3];
    CBauGruppe mBaugruppen[5];
    CBauteil mBauteile[10];
};

