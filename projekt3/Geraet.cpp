#include "stdafx.h"


double CGeraet::getPreis() {
    double preis = 0.0;
    for (int i = 0; i < mKomponentenBauteile.size(); i++) {
        CKomponenteAnzahl<CBauteil> komponente = mKomponentenBauteile[i];
        CObjekt *bauteilOderBaugruppe = komponente.getKomponente();
        if (bauteilOderBaugruppe != nullptr) {
            preis += bauteilOderBaugruppe->getPreis() * komponente.getAnzahl();
        }
    }
    return preis;
}

void CGeraet::ausgabeBaugruppen() {
    cout << "Baugruppen:" << endl;
    for (int i = 0; i < mKomponentenBaugruppen.size(); i++) {
        CBauGruppe *bauGruppe = dynamic_cast<CBauGruppe *>(mKomponentenBaugruppen[i].getKomponente());
        if (bauGruppe != nullptr) {
            bauGruppe->ausgabe();
            cout << "\tAnzahl " << mKomponentenBaugruppen[i].getAnzahl();
            cout << "\t" << bauGruppe->getPreis() * mKomponentenBaugruppen[i].getAnzahl() << " Euro " << endl;
            bauGruppe->ausgabeBauteile();
        }
    }
}

void CGeraet::ausgabeBauteile() {
    cout << "Einzel Bauteile:" << endl;
    for (int i = 0; i < mKomponentenBauteile.size(); i++) {
        //schlaegt ohne error fehl : entspricht java instanceof
        CBauteil *objPtr = dynamic_cast<CBauteil *> (mKomponentenBauteile[i].getKomponente());
        if (objPtr != nullptr) {
            cout << i << "\t";
            mKomponentenBauteile[i].getKomponente()->ausgabe();
            cout << "\tAnzahl " << mKomponentenBauteile[i].getAnzahl() << endl;
        }
    }
}

void CGeraet::addiereKomponente(CKomponente *komponente, int anzahl) {
    // entspricht instanceof und schlaegt ohne error fehl
    CBauteil *bauteilPtr = dynamic_cast<CBauteil *> (komponente);
    CBauGruppe *bauGruppePtr = dynamic_cast<CBauGruppe *> (komponente);
    if (bauteilPtr != nullptr)
        for (int i = 0; i < mKomponentenBauteile.size(); i++) {
            if (mKomponentenBauteile[i].getKomponente() == nullptr) {
                cout << anzahl << " " << bauteilPtr->getBezeichnung() << " hinzugefuegt" << endl;
                mKomponentenBauteile[i].setObjekt(bauteilPtr);
                mKomponentenBauteile[i].setAnzahl(anzahl);
                break;
            }
        }
    else if (bauGruppePtr != nullptr)
        for (int i = 0; i < mKomponentenBaugruppen.size(); i++) {
            if (mKomponentenBaugruppen[i].getKomponente() == nullptr) {
                cout << anzahl << " " << bauGruppePtr->getBezeichnung() << " hinzugefuegt" << endl;
                mKomponentenBaugruppen[i].setObjekt(bauGruppePtr);
                mKomponentenBaugruppen[i].setAnzahl(anzahl);
                break;
            }
        }
}

void CGeraet::loescheKomponente(CKomponente *komponente) {
    CBauteil *bauteilPtr = dynamic_cast<CBauteil *> (komponente);
    CBauGruppe *bauGruppePtr = dynamic_cast<CBauGruppe *> (komponente);
    if (bauteilPtr != nullptr)
        for (int i = 0; i < mKomponentenBauteile.size(); i++) {
            if (mKomponentenBauteile[i].getKomponente() == komponente) {
                cout << mKomponentenBauteile[i].getAnzahl() << " " << bauteilPtr->getBezeichnung() << " geloescht" << endl;
                mKomponentenBauteile[i].setObjekt(0);
                mKomponentenBauteile[i].setAnzahl(0);
                break;
            }
        }
    else if (bauGruppePtr != nullptr)
        for (int i = 0; i < mKomponentenBaugruppen.size(); i++) {
            if (mKomponentenBaugruppen[i].getKomponente() == komponente) {
                cout << mKomponentenBaugruppen[i].getAnzahl() << " " << bauGruppePtr->getBezeichnung() << " geloescht" << endl;
                mKomponentenBaugruppen[i].setObjekt(0);
                mKomponentenBaugruppen[i].setAnzahl(0);
                break;
            }
        }
}

CBauGruppe *CGeraet::waehleBaugruppe() {
    ausgabeBaugruppen();
    cout << endl << "Waehle Baugruppe : ";
    int baugruppePos;
    cin >> baugruppePos;
    CBauGruppe *baugruppe = baugruppePos < mKomponentenBaugruppen.size() ? mKomponentenBaugruppen[baugruppePos].getKomponente() : nullptr;
    if (baugruppe == nullptr)
        cout << "Fehlerhafte Auswahl" << endl;
    else
        cout << baugruppe->getBezeichnung() << " ausgewaehlt" << endl;
    return baugruppe;
}

CBauteil *CGeraet::waehleBauteil() {
    ausgabeBauteile();
    cout << endl << "Waehle Bauteil : ";
    int bauteilPos;
    cin >> bauteilPos;
    CBauteil *bauteil = bauteilPos < mKomponentenBauteile.size() ? mKomponentenBauteile[bauteilPos].getKomponente() : nullptr;
    if (bauteil == nullptr)
        cout << "Fehlerhafte Auswahl" << endl;
    else
        cout << bauteil->getBezeichnung() << " ausgewaehlt" << endl;
    return bauteil;
}
