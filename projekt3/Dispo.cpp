#include "stdafx.h"


CDispo::CDispo(void) {
    mBauteile[0] = CBauteil("Widerstand", 101, 0.8);;
    mBauteile[1] = CBauteil("Kondensator", 102, 0.9);
    mBauteile[2] = CBauteil("Induktivitaet", 103, 1);
    mBauteile[3] = CBauteil("LED", 104, 1.1);
    mBauteile[4] = CBauteil("Diode", 105, 1.2);
    mBauteile[5] = CBauteil("Brueckengleichrichter", 106, 1.3);
    mBauteile[6] = CBauteil("OP", 107, 1.4);
    mBauteile[7] = CBauteil("Transistor", 108, 1.5);
    mBauteile[8] = CBauteil("Trafo", 109, 22.5);
    mBauteile[9] = CBauteil("Platine", 110, 2.0);

    mBaugruppen[0].set("Netzteil", 8001);
    mBaugruppen[0].addiereBauteil(&mBauteile[9], 1);
    mBaugruppen[0].addiereBauteil(&mBauteile[0], 1);
    mBaugruppen[0].addiereBauteil(&mBauteile[1], 2);
    mBaugruppen[1].set("Verstaerker", 8002);
    mBaugruppen[1].addiereBauteil(&mBauteile[9], 1);
    mBaugruppen[1].addiereBauteil(&mBauteile[2], 3);
    mBaugruppen[1].addiereBauteil(&mBauteile[6], 2);
    mBaugruppen[2].set("Vorverstaerker", 8003);
    mBaugruppen[2].addiereBauteil(&mBauteile[9], 1);
    mBaugruppen[2].addiereBauteil(&mBauteile[2], 1);
    mBaugruppen[2].addiereBauteil(&mBauteile[6], 3);
    mBaugruppen[2].addiereBauteil(&mBauteile[7], 2);

    mGeraete[0].setBezeichnung("Steuerung");
    mGeraete[0].setId(9001);
    mGeraete[0].addiereKomponente(&mBaugruppen[0], 1);
    mGeraete[0].addiereKomponente(&mBauteile[3], 3);
    mGeraete[1].setBezeichnung("Super Verstaerker");
    mGeraete[1].setId(9002);
    mGeraete[1].addiereKomponente(&mBaugruppen[0], 1);
    mGeraete[1].addiereKomponente(&mBaugruppen[1], 3);
    mGeraete[1].addiereKomponente(&mBaugruppen[2], 3);
}


void CDispo::startProjekt() {
    string input = "";
    //6 -> exit
    while (input != "6") {
        cout << endl <<
                "Hauptmenue" << endl
                << "[1]\tInfo Bauteil" << endl
                << "[2]\tInfo Baugruppe" << endl
                << "[3]\tInfo Geraet" << endl
                << "[4]\tBearbeite Baugruppe" << endl
                << "[5]\tBearbeite Geraet" << endl
                << "[6]\tProgrammende" << endl
                << "Bitte Nummer waehlen : ";

        cin >> input;
        if (input.length() == 0)
            continue;
        switch (input[0]) {
            case '1': {
                infoBauteil();
                break;
            }
            case '2': {
                infoBaugruppe();
                break;
            }
            case '3': {
                infoGeraet();
                break;
            }
            case '4': {
                bearbeiteBaugruppe();
                break;
            }
            case '5': {
                bearbeiteGeraet();
                break;
            }
        }
    }
}

void CDispo::bauteileAusgeben() {
    cout << "Bauteil Liste: " << endl;
    for (int i = 0; i < sizeof(mBauteile) / sizeof(*mBauteile); i++) {
        cout << i << "\t";
        mBauteile[i].ausgabe();
        cout << endl;
    }
}

CBauteil *CDispo::waehleBauteil() {
    bauteileAusgeben();
    int id;
    cout << "Waehle Bauteil : ";
    cin >> id;
    if (id < 0 || id >= sizeof(mBauteile) / sizeof(*mBauteile)) {
        cout << "Ungueltige Eingabe" << endl;
        return 0;
    }
    else cout << mBauteile[id].getBezeichnung() << " ausgewaehlt" << endl;
    return &mBauteile[id];
}


CBauGruppe *CDispo::waehleBaugruppe() {
    cout << endl << "Baugruppe Liste: " << endl;
    for (int i = 0; i < sizeof(mBaugruppen) / sizeof(*mBaugruppen); i++) {
        cout << i << "\t";
        mBaugruppen[i].ausgabe();
        cout << endl;
    }
    int id;
    cout << "Waehle Baugruppe : ";
    cin >> id;
    if (id < 0 || id >= sizeof(mBaugruppen) / sizeof(*mBaugruppen)) {
        cout << "Ungueltige Eingabe" << endl;
        return 0;
    }
    else cout << mBaugruppen[id].getBezeichnung() << " ausgewaehlt" << endl;
    return &mBaugruppen[id];
}


CGeraet *CDispo::waehleGeraet() {
    cout << endl << "Geraet Liste: " << endl;
    for (int i = 0; i < sizeof(mGeraete) / sizeof(*mGeraete); i++) {
        cout << i << "\t";
        mGeraete[i].ausgabe();
        cout << endl;
    }
    int id;
    cout << "Waehle Geraet : ";
    cin >> id;
    if (id < 0 || id >= sizeof(mGeraete) / sizeof(*mGeraete)) {
        cout << "Ungueltige Eingabe" << endl;
        return nullptr;
    }
    else cout << mGeraete[id].getBezeichnung() << " ausgewaehlt" << endl;
    return &mGeraete[id];
}

void CDispo::infoBauteil() {
    CBauteil *bauteil = waehleBauteil();
    if (bauteil != 0) {
        cout << endl;
        bauteil->ausgabeMitPreis();
        cout << endl;
    }
}


void CDispo::infoBaugruppe() {
    CBauGruppe *bauGruppe = waehleBaugruppe();
    if (bauGruppe != 0) {
        cout << endl;
        bauGruppe->ausgabeMitPreis();
        cout << endl;
        bauGruppe->ausgabeBauteile();
    }
}

void CDispo::infoGeraet() {
    CGeraet *geraet = waehleGeraet();
    if (geraet != 0) {
        cout << endl;
        geraet->ausgabeMitPreis();
        cout << endl;
        geraet->ausgabeBaugruppen();
        geraet->ausgabeBauteile();
    }
}

void CDispo::bearbeiteBaugruppe() {
    cout << endl << "Bearbeitung einer Baugruppe gestartet" << endl;
    CBauGruppe *baugruppe = waehleBaugruppe();
    int befehl, bauteilPos, anzahl;
    if (baugruppe != 0) {
        cout << endl << endl <<
                "Menue Baugruppe bearbeiten" << endl
                << "[1]\tBauteil loeschen" << endl
                << "[2]\tBauteil hinzufügen" << endl
                << "[3]\tHauptmenue" << endl;
        cout << "Waehle Aktion : ";
        cin >> befehl;
        if (befehl == 3) return;

        CBauteil *bauteilPtr;
        switch (befehl) {
            case 1:
                baugruppe->ausgabeBauteile();
                cout << endl << "Ihre Wahl : ";
                cin >> bauteilPos;
                bauteilPtr = baugruppe->get(bauteilPos);
                baugruppe->loescheBauteil(bauteilPtr);
                break;
            case 2:
                bauteileAusgeben();
                cout << endl << "Waehle Bauteil : ";
                cin >> bauteilPos;
                if (bauteilPos < 0 || bauteilPos >= sizeof(mBauteile) / sizeof(*mBauteile)) {
                    cout << "Ungueltige Eingabe" << endl;
                    break;
                }
                bauteilPtr = &mBauteile[bauteilPos];
                cout << endl << "Waehle Anzahl : ";
                cin >> anzahl;
                baugruppe->addiereBauteil(bauteilPtr, anzahl);
                break;
            default:
                cout << "Ungueltige Eingabe" << endl;
        }
    }
}

void CDispo::bearbeiteGeraet() {
    cout << endl << "Bearbeitung eines Geraetes gestartet" << endl;
    string befehl = "";
    //5 -> exit
    CGeraet *derGeraet = waehleGeraet();
    while (befehl != "5") {
        cout << endl <<
                "Menue Geraet bearbeiten" << endl
                << "[1]\tBauteil hinzufuegen" << endl
                << "[2]\tBauteil loeschen" << endl
                << "[3]\tBaugruppe hinzufuegen" << endl
                << "[4]\tBaugruppe loeschen" << endl
                << "[5]\tZum Hauptmenue" << endl;
        cout << "Waehle Aktion : ";
        cin >> befehl;
        cout << endl;
        if (befehl.length() == 0)
            continue;
        switch (befehl[0]) {
            case '1': {
                CBauteil *bauteil = waehleBauteil();
                if (bauteil != nullptr) {
                    int anzahl = 0;
                    cout << "Waehle Anzahl : ";
                    cin >> anzahl;
                    derGeraet->addiereKomponente(bauteil, anzahl);
                }
                break;
            }
            case '2': {
                CKomponente *bauteil = derGeraet->waehleBauteil();
                if (bauteil != nullptr)
                    derGeraet->loescheKomponente(bauteil);
                break;
            }
            case '3': {
                CBauGruppe *bauGruppe = waehleBaugruppe();
                if (bauGruppe != nullptr) {
                    int anzahl = 0;
                    cout << "Waehle Anzahl : ";
                    cin >> anzahl;
                    derGeraet->addiereKomponente(bauGruppe, anzahl);
                }
                break;
            }
            case '4': {
                CBauGruppe *bauGruppe = waehleBaugruppe();
                if (bauGruppe != 0) derGeraet->loescheKomponente(bauGruppe);
                break;
            }
        }
    }
}
