// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <stdio.h>
#include <iostream>
#include <string>
#include <array>
#include <string.h>

using namespace std;

#include "Objekt.h"
#include "Komponente.h"
#include "KomponenteAnzahl.h"
#include "Bauteil.h"
#include "BauGruppe.h"
#include "Geraet.h"
#include "Dispo.h"

// TODO: reference additional headers your program requires here
