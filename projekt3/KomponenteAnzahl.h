#pragma once

template<class T>class CKomponenteAnzahl {
public:
    CKomponenteAnzahl() {
        mKomponente = 0;
        mAnzahl = 0;
    }

    CKomponenteAnzahl(T *komponente, int anzahl) {
        mKomponente = komponente;
        mAnzahl = anzahl;
    }

    void setObjekt(T *komponente) {
        mKomponente = komponente;
    }

    T *getKomponente() {
        return mKomponente;
    }

    void setAnzahl(int anzahl) {
        mAnzahl = anzahl;
    }

    int getAnzahl() {
        return mAnzahl;
    }

protected:
    T *mKomponente;
    int mAnzahl;
};

