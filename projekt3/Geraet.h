#pragma once

class CGeraet : public CObjekt {
public:
    CGeraet(void) {
    }

    double getPreis();

    CBauGruppe * waehleBaugruppe();

    CBauteil * waehleBauteil();

    void ausgabeBaugruppen();

    void ausgabeBauteile();

    void addiereKomponente(CKomponente *komponente, int anzahl);

    void loescheKomponente(CKomponente *komponente);

protected:
    //Besser als c-arrays. Hat zb. eine size() funktion etc.
    array<CKomponenteAnzahl<CBauteil>, 10> mKomponentenBauteile;
    array<CKomponenteAnzahl<CBauGruppe>, 10> mKomponentenBaugruppen;
};

