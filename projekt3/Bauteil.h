#pragma once

#include "stdafx.h"

class CBauteil : public CKomponente {
public:
    CBauteil(void) {
        setPreis(0.0);
    }

    CBauteil(char const *bezeichnung, int id, double preis) {
        setBezeichnung(bezeichnung);
        setId(id);
        setPreis(preis);
    }

    double getPreis() {
        return mPreis;
    }

    void setPreis(double preis) {
        mPreis = preis;
    }

protected:
    double mPreis;
};