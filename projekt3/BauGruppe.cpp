#include "stdafx.h"


double CBauGruppe::getPreis() {
    double preis = 0.0;
    double tempPreis = 0.0;
    for (int i = 0; i < mKomponenten.size(); i++) {
        CKomponenteAnzahl<CBauteil> komp = mKomponenten[i];
        CObjekt *bauteil = komp.getKomponente();
        if (bauteil != nullptr) {
            tempPreis = bauteil->getPreis();
            preis += tempPreis * komp.getAnzahl();
        }
    }
    return preis;
}

CBauteil *CBauGruppe::get(int pos) {
    return (CBauteil *) mKomponenten[pos].getKomponente();
}

void CBauGruppe::ausgabeBauteile() {
    cout << "Bauteile:" << endl;
    for (int i = 0; i < mKomponenten.size(); i++)
        if (mKomponenten[i].getKomponente() != nullptr) {
            cout << i << "\t";
            mKomponenten[i].getKomponente()->ausgabe();
            cout << "\tAnzahl " << mKomponenten[i].getAnzahl() << endl;
        }
}

void CBauGruppe::addiereBauteil(CBauteil *bauteil, int anzahl) {
    for (int i = 0; i < mKomponenten.size(); i++) {
        if (mKomponenten[i].getKomponente() == nullptr) {
            cout << anzahl << " " << bauteil->getBezeichnung() << " hinzugefuegt" << endl;
            mKomponenten[i].setObjekt(bauteil);
            mKomponenten[i].setAnzahl(anzahl);
            break;
        }
    }
}

void CBauGruppe::loescheBauteil(CBauteil *bauteil) {
    for (int i = 0; i < mKomponenten.size(); i++) {
        if (mKomponenten[i].getKomponente() == bauteil) {
            cout << mKomponenten[i].getAnzahl() << " " << bauteil->getBezeichnung() << " geloescht" << endl;
            mKomponenten[i].setObjekt(0);
            mKomponenten[i].setAnzahl(0);
            break;
        }
    }
}
