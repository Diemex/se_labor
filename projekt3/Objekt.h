#pragma once

class CObjekt {
public:
    CObjekt() {
        mId = 0;
        mBezeichnung = "";
    }

    int getId() {
        return mId;
    }

    char const *getBezeichnung() {
        return mBezeichnung;
    }

    void setBezeichnung(char const *bezeichnung) {
        mBezeichnung = bezeichnung;
    }

    void set(char const *bezeichnung, int id) {
        mId = id;
        mBezeichnung = bezeichnung;
    }

    void setId(int id) {
        mId = id;
    }

    virtual double getPreis() {
        return 0.0;
    }

    virtual void ausgabe() {
        cout << mId << "\t" << mBezeichnung;
    }

    virtual void ausgabeMitPreis() {
        cout << mId << "\t" << mBezeichnung << "\t" << getPreis() << " Euro ";
    }

protected:
    int mId;
    char const *mBezeichnung;
};

