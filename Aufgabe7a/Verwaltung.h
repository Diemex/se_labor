#include "Buch.h"

class Verwaltung {
public:

    Verwaltung();

    void printBook(unsigned long int id);

    void printBooks();

    void menu();

private :
    Buch books[6];
    int size = 0;
};