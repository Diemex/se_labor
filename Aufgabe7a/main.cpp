#include <iostream>
#include "Verwaltung.h"

using namespace std;

int main() {
    Verwaltung verwaltung;

    string input = "";
    while (input != "x" && input != "X") {
        verwaltung.menu();
        cin >> input;
        if (input.length() == 0)
            continue;
        switch (input[0]) {
            case 'a':
            case 'A':
                verwaltung.printBooks();
                break;
            case 'b':
            case 'B':
                unsigned int id = 0;
                cin >> id;
                verwaltung.printBook(id);
                break;
        }
    }
}