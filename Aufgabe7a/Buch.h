#include <stdio.h>
#include <string.h>

using namespace std;

class Buch {
public:
    Buch() {
        id = 0;
        loaned = false;
    }

    Buch(char *zTitel, unsigned long int hilfsSignatur) {
        strcpy(title, zTitel);
        id = hilfsSignatur;
        loaned = false;
    }

    char *getTitle() {
        return title;
    }

    unsigned long int getId() {
        return id;
    }

    bool getBorrowed() {
        return loaned;
    }

private :
    char title[40];
    unsigned long int id;
    bool loaned;
};