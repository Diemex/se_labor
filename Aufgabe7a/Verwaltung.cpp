#include "Verwaltung.h"
#include <iostream>

using namespace std;

Verwaltung::Verwaltung() {
    books[0] = Buch("Mathematik 1",101);
    books[1] = Buch("Physik 1",102);
    books[2] = Buch("Chemie 1",103);
    books[3] = Buch("Technische Mechanik 1",104);
    books[4] = Buch("Elektronik 1",105);
    books[5] = Buch("Informatik 1",106);
}

void Verwaltung::printBook(unsigned long int id) {
    cout << "Title: " << books[id - 101].getTitle() <<
            " \tId: " << books[id - 101].getId() <<
            " \tLoaned: " << books[id - 101].getBorrowed() << endl;
}

void Verwaltung::printBooks() {
    for (int i = 0; i < 6; i++) {
        printBook(101 + i);
    }
}

void Verwaltung::menu() {
    cout << "###################" << endl
            << "A/a - show all books" << endl
            << "B/b - find one book" << endl
            << "X/x - exit" << endl;
}