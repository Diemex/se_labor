EXEC = main.exe
SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.h*)
OBJECTS = $(SOURCES:.cpp=.o)

all: $(EXEC)

main.exe: $(OBJECTS)
	g++ -v $(OBJECTS) -o $(EXEC)

%.o: %.cpp $(HEADERS)
	g++ -c -v -g3 -O0 $< -o $@ -std=c++11

clean:
	rm -f $(EXEC) $(OBJECTS)