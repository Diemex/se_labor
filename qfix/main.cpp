#include <iostream>
#include "qfixSoccerBoard.h"

using namespace std;

int main() {
    qfixSoccerboard board;
    cout << "#############################" << endl
            << "driving back front and stopping" << endl;
    board.driveFrontBackStop();
    cout << "#############################" << endl
            << "driving until obstacle hit" << endl;
    board.driveUntilObstacleHit();
}