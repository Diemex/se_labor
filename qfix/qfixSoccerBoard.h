class qfixSoccerboard {
public:

    qfixSoccerboard();

    void ledOn(int i);

    void ledOff(int i);

    void motor(int i, int speed);

    void motorsOff();

    void msleep(int ms);

    void driveFrontBackStop();

    unsigned int analog(int i);

    void driveUntilObstacleHit();

protected:
    int motorspeed[3];
    bool ledStatus[2];
    int waitTime;
    //Aufgabe 3 + 4
    float totalDistance;
    float transferFactor;
    double distanceSensor[2];
    unsigned int analogIn[2];
    double distanceObstacleFront;
    double distanceObstacleBack;
    double transferFactorObstacles;
};