#import "qfixSoccerBoard.h"
#import <iostream>
#include <cstdlib>

using namespace std;

qfixSoccerboard::qfixSoccerboard() {

    //Aufgabe 1+2
    cout << "Constructor" << endl;
    for (int i = 0; i < 3; i++)
        ledStatus[i] = false;
    for (int i = 0; i < 2; i++)
        ledStatus[i] = false;
    waitTime = 0;
    totalDistance = 0;
    transferFactor = (float) 1 / (float) 200 / (float) 1000;
    //Aufgabe 4
    distanceSensor[0] = 0;
    distanceSensor[1] = 0;
    analogIn[0] = 0;
    analogIn[1] = 0;
    distanceObstacleFront = 1;
    distanceObstacleBack = -0.5;
    transferFactorObstacles = 1000;
}

void qfixSoccerboard::ledOff(int i) {
    cout << "led off: \t" << i << endl;
    ledStatus[i] = false;
}

void qfixSoccerboard::ledOn(int i) {
    cout << "led on: \t" << i  << endl;
    ledStatus[i] = true;
}

void qfixSoccerboard::motor(int i, int speed) {
    cout << "motor " << i << " on speed: \t" << speed << endl;
    motorspeed[i] = speed;
}

void qfixSoccerboard::motorsOff() {
    cout << "all motors off" << endl;
    for (int i = 0; i < 3; i++)
        motorspeed[i] = 0;
}

void qfixSoccerboard::msleep(int ms) {
    cout << "waiting for: \t" << ms << " total: \t" << waitTime << endl;
    waitTime += ms;
    float drivenDistance = motorspeed[0] * transferFactor * ms;
    totalDistance += drivenDistance;
    cout << "driven " << drivenDistance << " in " << ms << " millis" << endl
            << "total distance: " << totalDistance << endl;

}

void qfixSoccerboard::driveFrontBackStop() {
    ledOn(1);
    //front
    motor(0, 100);
    motor(1, -100);
    msleep(2000);
    motorsOff();
    msleep(1000);
    //back
    motor(0, -100);
    motor(1, 100);
    msleep(2000);
    motorsOff();
    ledOff(1);
}

//Aufgabe 4

unsigned int qfixSoccerboard::analog(int i) {
    distanceSensor[0] = distanceObstacleFront - totalDistance;
    distanceSensor[1] = (-1) * (distanceObstacleBack - totalDistance);
    analogIn[0] = distanceSensor[0] * transferFactorObstacles;
    analogIn[1] = distanceSensor[1] * transferFactorObstacles;
    int returnVal = analogIn[i] < 255 ? analogIn[i] : 255;
    cout << "analog sensor " << i << " returned " << returnVal << endl;
    return returnVal;
}

void qfixSoccerboard::driveUntilObstacleHit() {
    ledOn(1);

    //front
    motor(0, 100);
    motor(1, -100);
    //Drive until obstacle in front is too close
    while (true) {
        msleep(50);
        if (analog(0) <= 150) {
            motorsOff();
            break;
        }
    }
    msleep(1000);

    //back
    motor(0, -100);
    motor(1, 100);
    //Drive until obstacle at the back is too close
    while (true) {
        msleep(50);
        if (analog(1) <= 150) {
            motorsOff();
            break;
        }
    }

    ledOff(1);
}