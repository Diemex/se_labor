#include <iostream>

using namespace std;

class Resistor {
public:
    Resistor();

    void parallel(Resistor resistor1, Resistor resistor2);

    void printResistanceInOhm() {
        cout << "Resistance is: " << resistance << endl;
    }

    void inputResistor();

    void setResistance(float resistance);

protected:
    float resistance;
};