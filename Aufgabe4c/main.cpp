#include <iostream>
#include "Resistor.h"

using namespace std;

using namespace std;

int main() {
    cout << "1000 Ohm and 50 Ohm parallel";
    Resistor r, r1, r2;
    r1.setResistance(1000);
    r2.setResistance(50);
    r.parallel(r1, r2);
    r.printResistanceInOhm();
    //Frage 3
    r1.inputResistor();
    r2.inputResistor();
    r.parallel(r1, r2);
    r1.inputResistor();
    r2.parallel(r, r1);
    r2.printResistanceInOhm();
}