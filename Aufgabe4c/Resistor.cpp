#include <iostream>
#include "Resistor.h"

using namespace std;

Resistor::Resistor() {
    resistance = 0;
}

void Resistor::parallel(Resistor resistor1, Resistor resistor2) {
    resistance = resistor1.resistance * resistor2.resistance / (resistor1.resistance + resistor2.resistance);
}

void Resistor::inputResistor() {
    cout << "input resistance" << endl;
    cin >> resistance;
}

void Resistor::setResistance(float resistance) {
    this->resistance = resistance;
}