#include "Student.cpp"

using namespace std;

class AssayOffice {
private :
    Student mStudents[8];
    int mExamIds[6];

public :
    AssayOffice() {
        mExamIds[0] = 21;
        mExamIds[1] = 56;
        mExamIds[2] = 83;
        mExamIds[3] = 49;
        mExamIds[4] = 72;
        mExamIds[5] = 90;

        mStudents[0] = Student("Max Musterman", 11);
        mStudents[1] = Student("Elisa Müller", 13);
        mStudents[2] = Student("Hans Wurst", 17);
        mStudents[3] = Student("Ferdinand", 19);
        mStudents[4] = Student("Sigfried", 23);
        mStudents[5] = Student("Gudrun Maier", 29);
        mStudents[6] = Student("Hermann Alb", 31);
        mStudents[7] = Student("Heinrich Vogel", 37);
    }


    /**
    * Register exam for given student
    * @returns true if successful
    */
    bool registerExam(int student, int examId) {
        //Is the examId valid?
        for (int i = 0; i < 6; i++)
            if (mExamIds[i] == examId)
                break;
                //last exam id -> invalid id
            else if (i == 5)
                return false;
        //Does the student exist?
        for (int i = 0; i < 8; i++) {
            if (mStudents[i].getStudentId() == student) {
                mStudents[i].registerExam(examId);
                return true;
            }
        }
        return false;
    }

    /**
    * Drop exam for given student
    * @returns true if successful
    */
    bool dropExam(int student, int examId) {
        for (int i = 0; i < 8; i++)
            if (mStudents[i].getStudentId() == student)
                return mStudents[i].dropExam(examId);
        return false;
    }

    void printAllStudents() {
        cout << endl << "ALLE STUDENTEN" << endl;
        for (int i = 0; i < 8; i++) {
            mStudents[i].print();
        }
    }

    void printAllExams() {
        cout << endl << "ALLE PRUEFUNGEN" << endl;
        for (int i = 0; i < 6; i++) {
            cout << "Pruefung \t Pruefungsnummer " << mExamIds[i] << endl;
        }
    }

    void printRegisteredExams() {
        cout << "ALLE ANGEMELDETEN PRUEFUNGEN" << endl;
        for (int i = 0; i < 8; i++) {
            mStudents[i].printExams();
        }
    }
};