#include <iostream>

using namespace std;

class Student {
private :
    string mName;
    unsigned int mStudentId;
    int mRegisteredExams[8];


public:

    //Constructor for arrays
    Student() {
        mName = "";
        mStudentId = 0;
        //-1 = invalid exam id
        for (int i = 0; i < 8; i++)
            mRegisteredExams[i] = -1;
    }

    //Constructor
    Student(string name, int id) {
        mName = name;
        mStudentId = id;
        for (int i = 0; i < 8; i++)
            mRegisteredExams[i] = -1;
    }

    string getName() const {
        return mName;
    }

    /**
    * Get id "Matrikelnummer" of student
    */
    unsigned int getStudentId() const {
        return mStudentId;
    }

    void print() {
        cout << "Student\t" << mName << "\t Matrikelnummer " << mStudentId << endl;
    }

    void printExams() {
        cout << "Student\t" << mName << "\t hat die Pruefungen ";
        for (int i = 0; i < 8; i++)
            if (mRegisteredExams[i] != -1)
                cout << mRegisteredExams[i] << " ";
        cout << "angemeldet" << endl;
    }

    bool dropExam(int exam) {
        for (int i = 0; i < 8; i++)
            if (mRegisteredExams[i] == exam) {
                mRegisteredExams[i] = -1;
                return true;
            }
        return false;
    }

    void registerExam(int exam) {
        //Search for an empty spot in the array
        for (int i = 0; i < 8; i++)
            if (mRegisteredExams[i] == -1) {
                mRegisteredExams[i] = exam;
                break;
            }
    }
};