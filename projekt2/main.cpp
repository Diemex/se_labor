#include <iostream>
#include "AssayOffice.cpp"

using namespace std;

int main() {
    AssayOffice office;
    string input = "";
    //4 -> exit
    while (input != "4") {
        office.printAllStudents();
        office.printAllExams();
        cout << endl <<
                "Hauptmenue" << endl
                << "[1]\tAnmelden einer Pruefung" << endl
                << "[2]\tRuecktritt von einer Pruefung" << endl
                << "[3]\tAktuelle Pruefungen anzeigen" << endl
                << "[4]\tProgrammende" << endl << endl
                << "Bitte Nummer in [] auwaehlen!";

        cin >> input;
        if (input.length() == 0)
            continue;
        switch (input[0]) {
            //register/drop exam
            case '1':
            case '2': {
                int student, exam;
                office.printAllStudents();
                cout << "Matrikelnummer auswaehlen:";
                cin >> student;
                office.printAllExams();
                cout << "Pruefungsnummer auswaehlen:";
                cin >> exam;
                switch (input[0]) {
                    //register
                    case '1': {
                        bool success = office.registerExam(student, exam);
                        if (success)
                            cout << endl << "ANMELDUNG ERFOLGREICH DURCHGEFUEHRT!" << endl;
                        else
                            cout << endl << "ANMELDUNG KONNTE NICHT DURCHGEFUEHRT WERDEN!" << endl;
                        break;
                    }
                        //drop
                    case '2': {
                        bool success = office.dropExam(student, exam);
                        if (success)
                            cout << endl << "RUECKTRITT ERFOLGREICH DURCHGEFUEHRT!" << endl;
                        else
                            cout << endl << "RUECKTRITT KONNTE NICHT DURCHGEFUEHRT WERDEN!" << endl;
                        break;
                    }
                }
                break;
            }
                //print all exams
            case '3': {
                office.printRegisteredExams();
                break;
            }
        }
    }
    return 0;
}