#include <iostream>
#include <string>

using namespace std;

class CBruch {
public:

    void output();

    void add(CBruch bruch);

    void subtract(CBruch bruch);

    void invert();

    void multiply(CBruch bruch);

    void divide(CBruch bruch);

    void input(void);

    void set(long int nenner, long int zaehler);

    CBruch();

protected:
    long int zaehler, nenner;

    void kuerzen(void);

    int biggestCommonDivider(unsigned int first, unsigned int second);
};