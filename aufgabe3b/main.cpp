#include <iostream>
#include <vector>
#include <iterator>
#include "CBruch.hpp"

using namespace std;

void showMenu() {
    cout << "######################" << endl
            << "#   Expected input   #" << endl
            << "#   + - * /          #" << endl
            << "#   S to Set first   #" << endl
            << "#   X to Exit        #" << endl
            << "######################" << endl
            << "Befehl: ";
}

int main() {
    CBruch bruch1, bruch2;
    string input = "";
    while (input != "x" && input != "X") {
        showMenu();
        cin >> input;
        if (input.length() == 0)
            continue;
        switch (input[0]) {
            case '+':
                bruch2.input();
                bruch1.add(bruch2);
                bruch1.output();
                break;
            case '-':
                bruch2.input();
                bruch1.subtract(bruch2);
                bruch1.output();
                break;

            case '*':
                bruch2.input();
                bruch1.multiply(bruch2);
                bruch1.output();
                break;

            case '/':
                bruch2.input();
                bruch1.divide(bruch2);
                bruch1.output();
                break;

            case 's':
            case 'S':
                bruch1.input();
                break;

        }
    }
    return 0;
}