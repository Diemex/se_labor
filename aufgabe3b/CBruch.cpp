#include "CBruch.hpp"

using namespace std;


CBruch::CBruch() {
    zaehler = 0;
    nenner = 0;
}

void CBruch::add(CBruch bruch) {
    zaehler = zaehler * bruch.nenner + nenner * bruch.zaehler;
    nenner *= bruch.nenner;
    kuerzen();
}

void CBruch::subtract(CBruch bruch) {
    zaehler = zaehler * bruch.nenner - nenner * bruch.zaehler;
    nenner *= bruch.nenner;
    kuerzen();
}

void CBruch::invert() {
    long int temp = nenner;
    temp = zaehler;
    zaehler = nenner;
    nenner = temp;
}

void CBruch::multiply(CBruch bruch) {
    zaehler *= bruch.zaehler;
    nenner *= bruch.nenner;
    kuerzen();
}

void CBruch::divide(CBruch bruch) {
    bruch.invert();
    multiply(bruch);
    kuerzen();
}

void CBruch::input(void) {
    cout << "#### Input CBruch ####" << endl;
    cout << "Zaehler: ";
    cin >> zaehler;
    cout << "Nenner: ";
    cin >> nenner;
}

void CBruch::output() {
    cout << endl <<
            "Ergebnis: " << zaehler << "/" << nenner << endl;
}

void CBruch::set(long int zaehler, long int nenner) {
    this->nenner = nenner;
    this->zaehler = zaehler;
    kuerzen();
}

void CBruch::kuerzen(void) {
    unsigned int commonDivider(biggestCommonDivider(zaehler, nenner));
    zaehler /= commonDivider;
    nenner /= commonDivider;
}

int CBruch::biggestCommonDivider(unsigned int first, unsigned int second) {
    if (second == 0)
        return first;
    else return biggestCommonDivider(second, first % second);
}